#include    <iostream>
#include    "../include/Game.h"
#include    <string>

using namespace std;

int main(){
char choice;
Game * myGame = new Game();
do{
cout<<"===================================================="<<endl
    <<"                       Yahtzee                      "<<endl
    <<"               Please press S to start              "<<endl
    <<"                    or Q to quit                    "<<endl
    <<"===================================================="<<endl;
    cin>>choice;
    
    if(choice=='S'||choice=='s'){
        myGame->play();
	}
	else if (choice == 'q' || choice == 'Q') {
		break;
	}
 
    else{
        cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl
            <<"            That was not a valid character!       "<<endl
            <<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
    }
} while (choice != 'q' || choice != 'Q');

	delete myGame;
	system("pause");
    return 0;
}