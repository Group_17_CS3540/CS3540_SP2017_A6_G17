#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include <string>
#include "../include/die.h"

die myDie;

SCENARIO("User can roll a dice and get the correct values")
{
  GIVEN(myDie.roll())
  {    
    WHEN("user rolls die", "[rollDie]")
    {
        THEN("Die rolls")
        {
          REQUIRE(myDie.getVal() < 7);
          REQUIRE(myDie.getVal() > 0);
        }  
    }
  }
}
    