#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

#include "../include/die.cpp"
#include "../include/scoreCard.h"

TEST_CASE("Die"){
    Die die1;
    die1.roll();
    die1.get();
    die1.set(1);
}

SCENARIO("Die set","[Sets right]"){
    GIVEN("int to set"){
        Die die;
        int x=3;
        WHEN("Set is called"){
            die.set(x);
            THEN("Get function"){
               REQUIRE(die.get()==x);
            }
        }


    }
}
SCENARIO("Valid Roll"){
    GIVEN("Die object"){
        die die1;
        WHEN("rolled"){
            die1.roll();
            THEN("In range"){
                REQUIRE(die1.get()<7);
                REQUIRE(die1.get()>0);
            }
    }

}    

SCENARIO("Valid Roll"){
    GIVEN("Die object"){
        die die1;
        WHEN("rolled"){
            die1.roll();
            THEN("In range"){
                REQUIRE(die1.get()<7);
                REQUIRE(die1.get()>0);
            }
    }

}

TEST_CASE("scoreCard"){
    scoreCard myCard;
    myCard.getTotalScore();
}

SCENARIO("Creates a scorecard object"){
    GIVEN("scoreCardObject"){
        scoreCard myCard;
        WHEN("new is called"){
            myCard = new scoreCard;
            THEN("good"){
                REQUIRE(myCard.getTotalScore() == 0);
            }
        }
    }
}
SCENARIO("Get Total Score returns the correct score"){
    GIVEN("total score"){
        scoreCard myCard;
        myCard.total = 8;
        WHEN("get total is called"){
            myCard.getTotalScore();
            THEN("score = 8"){
                REQUIRE(myCard.getTotalScore() == 8);
            }
        }
    }
}
SCENARIO("Delete scoreCard"){
    GIVEN("scorecard object"){
        scoreCard myCard;
        WHEN("destructor is called"){
            delete myCard;
            THEN("should not be able to call score card"){
                REQUIRE(object.delete == true);
            }
        }
    }
}
SCENARIO("set score"){
    GIVEN("new score"){
        scoreCard myCard;
        int x = 8;
        WHEN("setScore is called"){
            myCard.setScore(1,8);
            THEN("can see scores"){
                REQUIRE(myCard.getScores(1) == 8);
            }
        }
    }
}