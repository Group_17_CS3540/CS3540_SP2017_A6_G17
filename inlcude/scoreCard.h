#ifndef SCORECARD_h
#define SCORECARD_h
#include <iostream>
#include <string>
#include "dice.h"

class scoreCard{
private:
    bool takenArray[14]{false};
    std::string theBigX[14] {"","","","","","","","","","","","","",""}; 
    int category=0;
    int scores[14]{};
public:
    scoreCard(){myDice= new dice();}    
    ~scoreCard(){delete myDice;}
    dice myDice;
    int total=0;
    int getTotalScore();//Total score output for the viewCard() function
    void viewCard();             //displays the card and scores
    void getScoreCard();         //displays dice roll, categories, prompts user input, does calculations, calls viewcard(basically the game)
    void setScores(int,int);
    void setCategoryStat(int);//sets a category to used-true
    int getScores(int position){return scores[position];}
    bool getCategoryStat(int); //returns a bool that determines whether of not a category has been used
};
#endif