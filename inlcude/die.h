#ifndef DIE_H
#define DIE_H
#include <iostream>
class die{
    public:
        int getVal(){return currentVal;}
        void setVal(int newVal){currentVal = newVal;}
        void roll();

    private:
        int currentVal;
};
#endif