#ifndef DICE_H
#define DICE_H
#include <iostream>
#include "die.h"

class dice{
    public:
        int arrayOfDice[6]{};
        ~dice();
        dice();
    private:
        die myDie1;
        die myDie2;
        die myDie3;
        die myDie4;
        die myDie5;
};

#endif