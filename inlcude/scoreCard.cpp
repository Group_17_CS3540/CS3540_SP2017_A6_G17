#include <iostream>
#include <string>
#include "ScoreCard.h"

void ScoreCard::getScoreCard(){
 	int sum = 0;
 	std::cout << "Please Select Your Category:" << std::endl
	 	 << "For the Lower Bounds (1-6) enter their corosponding number"<< std::endl
 		 << "For Three of a Kind enter 7" << std::endl
		 << "For Four of a Kind enter 8" << std::endl
 		 << "For Full house enter 9" << std::endl
 		 << "For Small Straight enter 10" << std::endl   // this displays the categories and their respective value
 		 << "For Large Straight enter 11" << std::endl
 		 << "For Chance enter 12" << std::endl
 		 << "For Yahtzee or Bonus enter 13" << std::endl;
 	std::cin >> category;
	 //error check here 
	 while(category > 13 || category < 1|| std::cin.fail()){
		 std::cout << "Invalid Option, please try again..." << std::endl;
		 std::cin >> category;
	 }
	category--;
	if(category >= 0 && category < 6 && !getCategoryStat(category)){
		std::cout<<takenArray[category]<<std::endl;
		if(getCategoryStat(category) == true){
			setScores(category,0);
		}
		else if (getCategoryStat(category) == false)//free
		{
			setScores(category,(category+1)*(myDice->arrayOfEachNumberRolled[category]));
		}
		setCategoryStat(category);
  	 }
 	else if(category ==6  && !getCategoryStat(category))    //Three of a kind
 	{
   		for(int k = 0; k < 6; k++)
		{
      		if(myDice->arrayOfEachNumberRolled[k] >= 3)
	  			{
			 		setScores(category,50);
		   			break;
	 			}
	 		 else
	   	 		setScores(category,0); 
	   	}
		setCategoryStat(category);
  	}
 	else if(category ==7  && !getCategoryStat(category))//Four of a kind
 	{
    	for(int k = 0; k < 6; k++)
	  	{
      		if(myDice->arrayOfEachNumberRolled[k] >= 4)
	   	 	{
	    		setScores(category,50);
				break;
	   		}
	  	 	else
	   		{ 
				setScores(category,0);
			}
	 	}  
	  setCategoryStat(category);
  	}
	else if(category == 8 && !getCategoryStat(category))//Full House
  	{
  		 for(int k = 0; k < 6; k++)
    		{
	  			if( myDice->arrayOfEachNumberRolled[k] == 2 || myDice->arrayOfEachNumberRolled[k] == 3)
	 			{ 
	   				for(int j = 0; j < 6; j++)
	   				{
	   
       				if((myDice->arrayOfEachNumberRolled[j] == 2 && myDice->arrayOfEachNumberRolled[k] == 3) || (myDice->arrayOfEachNumberRolled[j] == 3 && myDice->arrayOfEachNumberRolled[k] == 2))
	   				{
          				setScores(category,25);
       				}
	    			else
	   				{ 
						setScores(category,0); 
					}
	   				}
	  			}
			}  
		setCategoryStat(category);
 	 }
 	else if(category == 9 && !getCategoryStat(category))//Small Straight
  	{
    	if((myDice->arrayOfEachNumberRolled[0] >= 1 && myDice->arrayOfEachNumberRolled[1] >= 1 && myDice->arrayOfEachNumberRolled[2] >= 1 && myDice->arrayOfEachNumberRolled[3] >= 1) 
					|| (myDice->arrayOfEachNumberRolled[1] >= 1 && myDice->arrayOfEachNumberRolled[2] >= 1 && myDice->arrayOfEachNumberRolled[3] >= 1 && myDice->arrayOfEachNumberRolled[4] >= 1) 
					|| (myDice->arrayOfEachNumberRolled[2] >= 1 && myDice->arrayOfEachNumberRolled[3] >= 1 && myDice->arrayOfEachNumberRolled[4] >= 1 && myDice->arrayOfEachNumberRolled[5] >= 1) )
	 	{
	    	setScores(category,30);;
	  	}
		else
	  	{ 
		  	setScores(category,0);
	  	}
	  	setCategoryStat(category);
 	 }
 	else if(category == 10 && !getCategoryStat(category))//Large Straight
	  {
   
      	if((myDice->arrayOfEachNumberRolled[0] >= 1 && myDice->arrayOfEachNumberRolled[1] >= 1 && myDice->arrayOfEachNumberRolled[2] >= 1 
					&& myDice->arrayOfEachNumberRolled[3] >= 1 && myDice->arrayOfEachNumberRolled[4] >= 1) || (myDice->arrayOfEachNumberRolled[1] >= 1 && myDice->arrayOfEachNumberRolled[2] >= 1 
					&& myDice->arrayOfEachNumberRolled[3] >= 1 && myDice->arrayOfEachNumberRolled[4] >= 1 && myDice->arrayOfEachNumberRolled[5] >= 1) )
	 	 {
        
	    	setScores(category,40);
		
	  	}
	  	else{ 
		  	setScores(category,0); 
		 	}
	 	 setCategoryStat(category);
 	 }
 	else if(category == 11 && !getCategoryStat(category))//Chance
 	 {
	 	for(int k = 0; k < 6; k++)
		{
			sum+=myDice->arrayOfEachNumberRolled[k]*k+1;
	 	}
		setScores(category,sum);
	  	setCategoryStat(category);
	 
 	 }
 	else if((category == 12 && !getCategoryStat(category)) || ((getCategoryStat(category) && !getCategoryStat(category+1)&&category==12)) )//works for both Yahtzee and Bonus
  	{
    	if( myDice->arrayOfEachNumberRolled[0] == 5 || myDice->arrayOfEachNumberRolled[1] == 5 || myDice->arrayOfEachNumberRolled[2] == 5 
				|| myDice->arrayOfEachNumberRolled[3] == 5 || myDice->arrayOfEachNumberRolled[4] == 5 || myDice->arrayOfEachNumberRolled[5] )
		{
	  		setScores(category,50);
			setCategoryStat(category);
		}
		else if((myDice->arrayOfEachNumberRolled[0] == 5 || myDice->arrayOfEachNumberRolled[1] == 5 || myDice->arrayOfEachNumberRolled[2] == 5 
						|| myDice->arrayOfEachNumberRolled[3] == 5 ||myDice->arrayOfEachNumberRolled[4] == 5 || myDice->arrayOfEachNumberRolled[5] == 5) && getCategoryStat(category) && !getCategoryStat(category++))
		{
			setScores(category++,100);
	 		setCategoryStat(category++);
		}
		else
		{
			 setScores(category++,0);
		}
	  setCategoryStat(category);
    }
	
 	else if(getCategoryStat(category))//makes sure everything cant be chosen twice
	{
		//check for bonus
		if(category == 12 && getCategoryStat(12)){
			std::cout << "This category has already been called, please try again" << std::endl;  //error message for each category that recalls the function
	   		getScoreCard();
	   		return;
		}
	 else{
		std::cout << "This category has already been called, please try again" << std::endl;  //error message for each category that recalls the function
	   		getScoreCard();
	   		return;
	 	}
	}
	else{	
		std::cout<<"GameBreaking Error, What did you do?!"<<std::endl;
	}

	viewCard();
 	return;
 }
 void ScoreCard::viewCard()
 {
   std::cout <<" Here is your current Score:" << std::endl;
   std::cout << theBigX[0] <<"One's': "<<getScores(0)<<std::endl
  		<< theBigX[1] <<"Two's': "<<getScores(1)<<std::endl
  		<< theBigX[2] <<"Three's': "<<getScores(2)<<std::endl
 	  	<< theBigX[3] <<"Four's': "<<getScores(3)<<std::endl
 		<< theBigX[4] <<"Five's': "<<getScores(4)<<std::endl
   		<< theBigX[5] <<"Six's': "<<getScores(5)<<std::endl
   		<< theBigX[6] <<" Three of a Kind: " << getScores(6) << std::endl
   		<< theBigX[7] <<" Four of a Kind: " << getScores(7) << std::endl
   		<< theBigX[8]<<" Full House: " << getScores(8) << std::endl
   		<< theBigX[9]<<" Small Straight: " << getScores(9) << std::endl
   		<< theBigX[10]<<" Large Straight: " << getScores(10) << std::endl
   		<< theBigX[11] <<" Chance: " << getScores(11) << std::endl
		<< theBigX[12]<<" Yahtzee: " << getScores(12)<< std::endl
   		<< theBigX[13]<<" Bonus: " << getScores(13) << std::endl;
   std::cout << "Total Score: " << getTotalScore() << std::endl;   
 }
 bool ScoreCard::getCategoryStat(int position){
	 return takenArray[position];
 }
 	//sets the requested position to true and adds the X to the designated theBigX array
 void ScoreCard::setCategoryStat(int position){
				std::cout<<"setCategoryStat"<<std::endl; 
	takenArray[position]=true;
				std::cout<<"takenArray modded"<<std::endl;
	theBigX[position]="X";
				std::cout<<"bigX modded"<<std::endl;
 }
 void ScoreCard::setScores(int position,int newVal){
			std::cout<<"made it into setScores"<<std::endl;
	if(position!=12){
		scores[position]=newVal;
	}
	else{
		if(getCategoryStat(position))//if yahtzee has already been called
		{
			scores[position+1]=newVal;
		}
		else scores[position]=newVal;
	}
 }
int ScoreCard::getTotalScore() 
{
    int total=0;        
        for(int i=0;i<=13;i++){
         total+=getScores(i);
        }
    return total;    
}